'use strict'
const {utils} = require(__api);

var test = async function(request) {
    return  "test"
}

module.exports = {
    method: 'GET'
    , api: '/test'
    , callback: test
    , middlewares: [
        "jwt"
    ]
};