const jwt = require('jsonwebtoken');
const moment = require('moment');
const logger = require('../lib/logger')

const BEARER_REGEXP = /^Bearer/i;
const SERVER_SECRET = process.env.SERVER_SECRET;

const SESSION_TEMPERING_ERROR = 'Session header tempering';
const SESSION_ACCESS_ERROR = 'Invalid session';
const SESSION_EXPIRE_ERROR = "Expired token";
const NO_TOKEN = "No token";

var extractJwtFormRequestHeader = function(request) {
    const headers = request.headers;
    const hasAuthorizationHeader = headers.hasOwnProperty('authorization');
    const hasBearerString = hasAuthorizationHeader && BEARER_REGEXP.test(headers.authorization.toString());

    return hasAuthorizationHeader && hasBearerString && headers.authorization.replace('Bearer ', '');
}


var authorizeJWT = async function(request) {
    let session;

    let remoteIP = request.connection.remoteAddress
    let remotePort = request.connection.remotePort

    // helpers for unix socket bindings with no forward
    if (!remoteIP && !remotePort) {
      remoteIP = '0.0.0.0'
      remotePort = '0'
    }

    if (request.headers['x-forwarded-for']) {
      let parts
      let forwardedIp = request.headers['x-forwarded-for'].split(',')[0]
      if (forwardedIp.indexOf('.') >= 0 || (forwardedIp.indexOf('.') < 0 && forwardedIp.indexOf(':') < 0)) {
        // IPv4
        forwardedIp = forwardedIp.replace('::ffff:', '') // remove any IPv6 information, ie: '::ffff:127.0.0.1'
        parts = forwardedIp.split(':')
        if (parts[0]) { remoteIP = parts[0] }
        if (parts[1]) { remotePort = parts[1] }
      } else {
        // IPv6
        parts = api.utils.parseIPv6URI(forwardedIp)
        if (parts.host) { remoteIP = parts.host }
        if (parts.port) { remotePort = parts.port }
      }

      if (request.headers['x-forwarded-port']) {
        remotePort = request.headers['x-forwarded-port']
      }
    }

    const token = extractJwtFormRequestHeader(request);
    if (! token) {
        throw new Error(NO_TOKEN);
    }

    session = jwt.decode(token, {complete: true});

    if (! session) {
        logger.info(`${SESSION_ACCESS_ERROR} failed to decode token ${token}`);
        throw new Error(SESSION_ACCESS_ERROR);
    }

    if (session.header.alg !== 'HS256' || session.header.typ !== 'JWT') {
        logger.info(`${SESSION_TEMPERING_ERROR} -> alg: ${session.header.alg} type-> ${session.header.typ}`);
        throw new Error(SESSION_TEMPERING_ERROR);
    }

    try {
        session = jwt.verify(token, `${SERVER_SECRET}`);
    } catch (error) {
        logger.info(`${SESSION_ACCESS_ERROR}, failed to verify token ${token}`);
        throw new Error(SESSION_ACCESS_ERROR);
    }

    if(moment().unix() > session.expire) {
        throw new Error(SESSION_EXPIRE_ERROR);
    }

    request.session = session;
}

module.exports = {
    name: "jwt"
    , preProcessor: authorizeJWT
}