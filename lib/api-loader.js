const path = require('path')
const fs   = require('fs');

const utils = require('./utils');
const logger = require('./logger');

const SERVER_MODE = process.env.NODE_ENV || 'development';

const api = {
	restful: {},
	config: {},
	classes: {},
	middlewares: {},
	initializers: {},
	logger: logger,
	utils: utils
}

function loadModules(dirsArray, hashmap, filesType) {
	hashmap = (hashmap) ? hashmap : api;
	filesType = (!filesType) ? [".js"] : filesType;
	dirsArray = (dirsArray instanceof Array) ? dirsArray : [dirsArray];

	let scanDir = (currentPath) => {
		let allFiles = fs.readdirSync(currentPath);
		let folders = [];

		for(var i in allFiles) {
			var curFile = path.join(currentPath, allFiles[i]);
			if(fs.statSync(curFile).isFile() && filesType.indexOf(path.extname(curFile)) != -1) {
				let currFile = path.basename(allFiles[i], path.extname(allFiles[i]));
				let fileExports = null;
				try {
					fileExports = require(path.join(currentPath, currFile));
				} catch(error) {
					//TODO: throw error;
					console.log(error);
				}
                if(!fileExports) {
                    continue;
				}
				try {
					// console.log(fileExports.prototype.constructor.name);
					hashmap[fileExports.prototype.constructor.name] = fileExports;
				} catch(error) {
					if(fileExports.constructor.name === 'Object') {
						// console.log(currFile);
						hashmap[currFile] = fileExports;
					} else {
						// console.log(fileExports.constructor.name);
						hashmap[fileExports.constructor.name] = fileExports;
					}
				}
			} else {
				folders.push(allFiles[i]);
			}
		}
		for(var i in folders) {
			var curFile = path.join(currentPath, folders[i]);
			if(!fs.statSync(curFile).isFile()) {
				let nextDir = path.basename(curFile);
				scanDir(path.join(currentPath, nextDir));
			}
		}
	};

	for(let i=0; i<dirsArray.length; i++) {
		scanDir(path.join(__absolutePath, dirsArray[i]));
	}
}

const loadConfigs = function() {
	const generateConfig = (configKey, config) => {
		if(!config) {
			return {};
		}
		let configObject = {};
		if(config.default && typeof(config.default[configKey]) === 'function') {
			configObject = {...configObject, ...config.default[configKey](api)};
		}
		if(config[SERVER_MODE] && typeof(config[SERVER_MODE][configKey]) === 'function') {
			configObject = {...configObject, ...config[SERVER_MODE][configKey](api)};
		}
		return configObject;
	}

	loadModules('config', api.config);
	for(let configKey in api.config) {
		try {
			api.config[configKey] = generateConfig(configKey, api.config[configKey]);
		} catch(error) {
			//TODO: think what to do ! stop the server ?
			console.log(error);
		}
	}
}

//TODO: load only configs and initializers
const load = function() {

    //load system classes
    loadModules(["lib/classes"], api.classes);

	//load system classes
	loadModules(["initializers"], api.initializers);

	//load configs
	loadConfigs();

    //load api requests handlers 
    loadModules(["api"], api.restful);
    
    //load middlewares
    loadModules(["middlewares"], api.middlewares);
}

// const loadAPI = async function() {

// 	//load configs
// 	loadConfigs();

//     //load api requests handlers 
//     loadModules(["api"], api);
    
//     //load middlewares
//     loadModules(["middlewares"], middlewares);
// }

module.exports = {
    load
    , api
};