"use strict"
const chalk = require('chalk');
const cliProgress = require('cli-progress');

const  {api} = require(__api);
const {utils} = api;

function cli() {
    this.name = "default";
    this.pbar = null;
    this.pbar_counter = 0;
    this.initializers = [];
}
var self = cli.prototype;

self.run = function() {
    return Promise.all('');
}

self.startProgressBar = function(count) {
    if(this.pbar != null) {
        this.stopProgressBar();
    }
    this.pbar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);
    this.pbar.start(count || 0, 0);
}

self.stopProgressBar = function() {
    if(this.pbar != null) {
        this.pbar.stop();
        this.pbar = null;
    }
}

self.setProgress = function(count) {
    let _count = count || 1;
    this.pbar_counter += _count;
    this.pbar.update(this.pbar_counter);
}

//TODO: move the progression bar here
self.execute = async function(printOff, startInitializersHandler, stopInitializersHandler) {
    if(printOff != true) {
        utils.printTitle(`Command => ${this.name}`);
    }
    try {
        await startInitializers(this, startInitializersHandler);
        await this.run();
        await stopInitializers(this, stopInitializersHandler);
        if(printOff != true) {
            console.log(`\n[${chalk.yellow(this.name)}] => ${chalk.green.bold('Done')}\n`);
        }
    } catch(error) {
        this.stopProgressBar();
        console.log(`\n[${chalk.yellow(this.name)}] => ${chalk.red('Error')}: ${error.message}\n`);
    }
    process.exit();
}

var startInitializers = async function(thisArg, startInitializersHandler) {
    await startInitializersHandler(thisArg.initializers);
}

var stopInitializers = async function(thisArg, stopInitializersHandler) {
    await stopInitializersHandler(thisArg.initializers);
}

module.exports = cli;