const winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');

const SIX_MONTH_LIFESPAN = 180;

let _logger = {transports: []};

_logger.transports.push(new DailyRotateFile({
    level: 'info',
    datePattern: 'DD.MM.YYYY',
    filename: 'var/log/decreption-api-%DATE%.log',
    maxDays: SIX_MONTH_LIFESPAN,
    format: winston.format.combine(
      winston.format.timestamp({}),
      winston.format.json()
    )
}));

var logger = winston.createLogger(_logger);

  module.exports = logger;