const QuickEncrypt = require('quick-encrypt');
const NodeRSA = require('node-rsa');
const jwt = require('jsonwebtoken');
const path = require('path');
const moment = require('moment');
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const {createHash} = require('crypto');
const chalk = require('chalk');
const stream = require('stream');

function Utils() {}
var self = Utils.prototype;

const ENCRYPTED_DATA_LENGTH = 170
const BULK_SIZE = 60000;

self.sha1sum = function(input) {
    return createHash('sha1').update(input.toString()).digest('hex');
}

//TODO: validate type
self.lastChars = function(data, number) {
    let substr = data.substring(data.length-number, data.length);
    return substr;
}

self.generateBlindIndex = function(data) {
    if(!data) {
        return '';
    }
    let last5 = this.lastChars(data, 5);
    return this.lastChars(this.sha1sum(last5), 10);
}

self.promisify = function(dbClient, query) {
    //TODO: validate the func param
    return new Promise((resolve, reject) => {
        dbClient.query(query, (error, result) => {
            if(error) {
                return reject(error);
            }
            resolve(result);
        });
        // resolve(10);
    });
}


self.tryRunFunction = async function(callback, data) {
    const isAsync = callback.constructor.name === "AsyncFunction";
    if(isAsync) {
        await callback(data);
    } else if(typeof callback === "function") {
        callback(data);
    }
}

//TODO: case sensitive
self.getQueryRecordsCount = async function(query, dbClient) {
    let regexU = /LIMIT ([0-9]+)/g;
    let regexL = /limit ([0-9]+)/g;
    let regexUResult = regexU.exec(query);
    let regexLResult = regexL.exec(query);
    if(regexUResult && regexUResult[1]) {
        return regexUResult[1];
    } else if(regexLResult && regexLResult[1]) {
        return regexLResult[1];
    }
    let countQuery = (query.replace(/[\r\n\x0B\x0C\u0085\u2028\u2029]+/g, " ").replace(/SELECT.*FROM/, 'SELECT count(*) as recordsCount FROM'));
    console.log(countQuery)
    let count = await this.promisify(dbClient, countQuery);
    return count[0].recordsCount;
}

//TODO: improve the way we pass wherestatment
self.doInBulks = async function(privateKey, tableName, bulkHandler, db, whereStatment) {
    if(!privateKey || !tableName || !bulkHandler) {
        return false;
    }
    console.log(`[gathering data from db: ${tableName}]`);
    let recordsCount = await db.query(`SELECT count(*) as count FROM ${tableName}${whereStatment ? ' ' + whereStatment : ';'}`);

    console.log(`[${recordsCount[0].count}] records to be changed`);
    let count = recordsCount[0].count, index = 0, changed = 0;

    while(count > 0) {
        let counter = await bulkHandler(index, BULK_SIZE, privateKey);
        count -= BULK_SIZE;
        index += BULK_SIZE;
        if(counter) {
            changed += counter;
            console.log(`[records changed: ${chalk.yellow(counter)}] [${changed} total]`);
        }
    }
    console.log(`[records changed ${tableName}: ${chalk.yellow(changed)}]`);
    return true;
}

self.sleep = function(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

self.ltrim = function(str) {
    if(!str) return str;
    return str.replace(/^\s+/g, '');
}

self.validateNumber = function(phone) {
    let formattedPhone = phone;
    let phone_is_valid = 0;
    try {
        formattedPhone = ltrim(formattedPhone);
        if(!formattedPhone.startsWith('+')) {
            formattedPhone = '+' + formattedPhone;
        }
        formattedPhone = phoneUtil.parse(formattedPhone, "");
        phone_is_valid = phoneUtil.isValidNumber(formattedPhone) ? 1 : 0;
    } catch(error) {
        //phone is not valid
    }
    return phone_is_valid;
}


self.createJwtSession = function(payload, privateKey, expireTime) {
    const expire = moment().add(expireTime, 'millisecond');
    const defaultPayload = {
        issuer: config.jwt.issuer,
        expire: expire.unix(),
        date: expire.format()
    };

    payload = Object.assign({}, defaultPayload, payload);
    return {
        payload: payload,
        expire: expire,
        token: jwt.sign(payload, privateKey, { algorithm: 'RS256'})
    };
}

self.isValidPrivateKey = function(privateKey) {
    if(!privateKey || privateKey === '') {
        return false;
    }
    return true;
}

self.getArray = function(nodes) {
    var nodesArr = nodes;
    if(!(nodesArr instanceof Array)) {
        if(typeof nodes == "undefined") {
            nodesArr = [];
        } else {
            nodesArr = [nodes];
        }
    }
    return nodesArr;
}

self.isBase64 = async function(str, privateKey) {
    if(str && str.length > 170) {
        return true;
    }
    try {
        let decryptionResult = await this.decrypt(str, privateKey);
        if(decryptionResult) {
            return true;
        }
        return false;
    } catch(error) {
        return false;
    }
}

self.printTitle = function(text) {
    console.log(`₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋₋\n ${text} \n⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻`);
}

var utils = new Utils();
module.exports = utils;