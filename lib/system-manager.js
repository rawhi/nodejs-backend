require('dotenv').config();

const apiLoader = require('./api-loader');
const path = require('path');

global.__absolutePath = path.dirname(__dirname);
global.__api = path.join(__dirname, 'api-loader');

const NetworkManager = require('./network-manager');

function SystemManager() {
    apiLoader.load();
}
var self = SystemManager.prototype;

self.startInitializers = async function(initializersList) {
    let initializers = apiLoader.api.initializers;
    for(let i in initializers) {
        try {
            if(initializersList.filter(item => item.toLowerCase() == initializers[i].name.toLowerCase()).length > 0) {
                await initializers[i].start();
            }
        } catch(error) {
            console.log('\n' + error);
            apiLoader.api.logger.error(error);
        }
    }
}

self.stopInitializers = async function(initializersList) {
    let initializers = apiLoader.api.initializers;
    for(let i in initializers) {
        try {
            if(initializersList.includes(initializers[i].name)) {
                await initializers[i].stop();
            }
        } catch(error) {
            console.log('\n' + error);
            apiLoader.api.logger.error(error);
        }
    }
}


self.init = async function(app) {
    //TODO: maybe need to start the initializers?
    //TODO: run the initializers unless you have false in the .env file
    NetworkManager.initAPI(app);
}

const system = new SystemManager();
module.exports = system;