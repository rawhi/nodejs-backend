'use strict'
const {api} = require(__api);

function NetworkManager() {
}
var self = NetworkManager.prototype;

self.initAPI = function(app) {
    let {restful, middlewares, logger} = api;
    if(!app) {
        return;
    }
    Object.values(restful).map(apiTuple => {
        if(Object.keys(apiTuple).length == 0) {
            return;
        }
        //TODO: can be  optimized
        let apiMiddlewares = [];
        if(apiTuple.middlewares) {
            apiMiddlewares = Object.values(middlewares).filter(middleware => apiTuple.middlewares.includes(middleware.name));
        }
        
        let apiPreMiddlewares = apiMiddlewares.filter(middleware => !!middleware.preProcessor);
        // let apiPostMiddlewares = apiMiddlewares.filter(middleware => !!middleware.postProcessor);

        apiPreMiddlewares.map(middleware => app.use(apiTuple.api, function (request, response, next) {
            middleware.preProcessor(request)
            .then(async () => {
                next();
            })
            .catch(exp => {
                logger.error(exp.message);
                response.status(500).send(exp.message);
                response.end();
            });
        }));
        
        app[apiTuple.method.toLowerCase()](apiTuple.api, (request, response) => {
            apiTuple.callback(request)
            .then(result => {
                response.json(result);
                response.end();
            })
            .catch(exp => {
                logger.error(exp.message);
                response.status(500).send(exp.message);
                response.end();
            });
        });
    });
}

var nm = new NetworkManager();
module.exports = nm;