#! /usr/bin/env node
const system = require('./system-manager');
const path  = require('path');
const chalk = require('chalk');
const {api}  = require(__api);

const {logger} = api;

// Get arguments
const [,, ...args] = process.argv;

(async () => {
    let cli = null, category = null, pathToScript = null;

    try {
        category = args[0], command  = args[1], printOff = args[2];
        if(!category || !command) {
            //TODO:
            return;
        }
        pathToScript = path.join(__dirname, category, command);
        cli = require(path.join(__absolutePath, 'cli', category, command));
        //TODO: improve by receiving another param to detect which initializer this script needs.
        (new cli()).execute(printOff, system.startInitializers, system.stopInitializers);
    } catch(error) {
        console.log(`\n${chalk.red('Error')}: can't find module => ${pathToScript} => ${error}`);
        process.exit();
    }
})();
