const process = require("process")
const rdl = require("readline")

class LoadingBar {
    constructor(size) {
        this.size = size
        this.cursor = 0
        this.timer = null
    }
    start() {
        process.stdout.write("\x1B[?25l")
        process.stdout.write("[")
        for (let i = 0; i < this.size-1; i++) {
            process.stdout.write("-")
        }
        process.stdout.write("]")
        this.cursor = 1
        rdl.cursorTo(process.stdout, this.cursor);
        this.timer = setInterval(() => {
            process.stdout.write("=")
            this.cursor++;
            if (this.cursor >= this.size) {
                clearTimeout(this.timer)
                process.stdout.write("\x1B[?25h")
            }
        }, 100)
    }
}

process.on('exit', exitHandler.bind(null,{cleanup:true}));

function exitHandler(options, exitCode) {
    console.log('\n')
}

const ld = new LoadingBar(50);
ld.start();

const ld2 = new LoadingBar(50);
ld2.start();
