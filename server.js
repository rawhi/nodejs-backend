const system        = require('./lib/system-manager');
const port          = process.env.API_PORT || 5656;
const express       = require('express');
const app           = express();
const Logger        = require("./lib/logger");

app.listen(port, '0.0.0.0');
app.use(express.json());

(async () => {
    await system.init(app);
    Logger.info('Listening to port', port);
})();

module.exports = app;