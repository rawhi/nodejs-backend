/*global Promise */
'use strict';
const mysql = require('promise-mysql');
const {api} = require(__api);
const {utils} = api;
const chalk = require('chalk');

const CHECK_CONNECTION_QUERY = 'SELECT "connected" as `status`';
//TODO: put it in config
const BULK_SIZE = 60000;


function MasterDB() {
    this.name = 'MasterDB';
    this.loadPriority = 1000;
    this.startPriority = 1000;
    this.stopPriority = 1000;
}
var self = MasterDB.prototype;

self.start = async () => {
    api.masterDB = await mysql.createPool(api.config.masterdb);
    // console.log(api.masterDB);
    api.masterDB.on('error', (error) => api.logger.error(`[MasterDB]: Server connection failed ${error}`));
    api.masterDB.on('connection', () => api.logger.info('[MasterDB]: Server connected'));

    api.SQLMaster = new SQLMaster(api.masterDB);
    try {
        await api.masterDB.query(CHECK_CONNECTION_QUERY);
        api.logger.info('[MasterDB]: Check connection OK');
    } catch(error) {
        api.logger.error(`[MasterDB]: Failed to Start ${error}`);
        throw new Error(`[MasterDB]: Failed to Start ${error}\n`);
    }
}

self.stop = async () => {
    async function disconnect() {
        return !api.masterDB.end
            ? Promise.resolve(false)
            : new Promise((resolve, reject) => api.masterDB.end((error) => !error ? resolve(true) : reject(error)));
    }

    try {
        await disconnect();
        api.logger.info('[MasterDB] Stopped');
    } catch(error) {
        api.logger.error(`[MasterDB] Failed on disconnect ${error}`);
        throw new Error(`[MasterDB] Failed on stop`);
    }
}

//TODO: start use this.masterDB instead of api.masterDB
function SQLMaster(masterDB) {
    this.loadPriority = 1000;
    this.startPriority = 1000;
    this.stopPriority = 1000;
    this.masterDB = masterDB;
}
var sqlMaster = SQLMaster.prototype;

module.exports = new MasterDB();
