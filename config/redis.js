// let REDIS_HOST     = process.env.REDIS_HOST || '127.0.0.1';
// let REDIS_PORT     = process.env.REDIS_PORT || 6379;
// let REDIS_DB       = process.env.REDIS_DB || 0;
// let REDIS_PASSWORD = process.env.REDIS_PASSWORD || null;
// const maxBackoff = 1000;

// if (process.env.REDIS_URL) {
//     REDIS_PASSWORD = process.env.REDIS_URL.match(/redis:\/\/.*:(.*)@.*:\d*$/i)[1];
//     REDIS_HOST     = process.env.REDIS_URL.match(/redis:\/\/.*:.*@(.*):\d*$/i)[1];
//     REDIS_PORT     = parseInt(process.env.REDIS_URL.match(/redis:\/\/.*:.*@.*:(\d*)$/i)[1]);
// }

// exports['default'] = {
//     redis: (api) => {
//         // konstructor: The redis client constructor method.  All redis methods must be promises
//         // args: The arguments to pass to the constructor
//         // buildNew: is it `new konstructor()` or just `konstructor()`?

//         function retryStrategy(times) {
//             if (times === 1) {
//                 const error = 'Unable to connect to Redis - please check your Redis config!';
//                 if (process.env.NODE_ENV === 'test') {
//                     console.error(error);
//                 } else {
//                     api.log('CONFIG: Redis error on start', 'error', error);
//                 }
//                 return 5000;
//             }
//             return Math.min(times * 50, maxBackoff);
//         }

//         return {
//             enabled: true,

//             '_toExpand': false,
//             client: {
//                 konstructor: require('ioredis'),
//                 args: [{port: REDIS_PORT, host: REDIS_HOST, password: REDIS_PASSWORD, db: REDIS_DB, retryStrategy: retryStrategy}],
//                 buildNew: true
//             },
//             subscriber: {
//                 konstructor: require('ioredis'),
//                 args: [{port: REDIS_PORT, host: REDIS_HOST, password: REDIS_PASSWORD, db: REDIS_DB, retryStrategy: retryStrategy}],
//                 buildNew: true
//             },
//             tasks: {
//                 konstructor: require('ioredis'),
//                 args: [{port: REDIS_PORT, host: REDIS_HOST, password: REDIS_PASSWORD, db: REDIS_DB, retryStrategy: retryStrategy}],
//                 buildNew: true
//             }
//         };
//     }
// };
