'use strict';
const MASTER_DB_HOST                = process.env.MASTER_DB_HOST || '127.0.0.1';
const MASTER_DB_PORT                = process.env.MASTER_DB_PORT || 3306;
const MASTER_DB                     = process.env.MASTER_DB;
const MASTER_DB_USER                = process.env.MASTER_DB_USER;
const MASTER_DB_PASSWORD            = process.env.MASTER_DB_PASSWORD;
const MASTER_DB_QUEUE_LIMIT         = process.env.MASTER_DB_QUEUE_LIMIT || 10;
const MASTER_DB_PING_INTERVAL       = process.env.MASTER_DB_PING_INTERVAL || 15000;
const MASTER_DB_CONNECTION_LIMIT    = process.env.MASTER_DB_CONNECTION_LIMIT || 5;

exports.default = {
    masterdb: {
        host: null,
        port: null,
        database: null,
        user: null,
        password: null,
        queueLimit: null,
        pingInterval: null,
        connectionLimit: null
    }
};

exports.development = {
    masterdb: () => {
        const defaultDb     = 'forex';
        const defaultUser   = 'root';
        const defaultPass   = 'rawhi@lab';

        return {
            host:            MASTER_DB_HOST,
            port:            MASTER_DB_PORT,
            database:        MASTER_DB || defaultDb,
            user:            MASTER_DB_USER || defaultUser,
            password:        MASTER_DB_PASSWORD || defaultPass,
            queueLimit:      MASTER_DB_QUEUE_LIMIT,
            pingInterval:    MASTER_DB_PING_INTERVAL,
            connectionLimit: MASTER_DB_CONNECTION_LIMIT
        };
    }
};

exports.production = {
    masterdb: () => {
        if (!MASTER_DB || !MASTER_DB_USER || !MASTER_DB_PASSWORD ) {
            throw new Error('MASTER_DB Config: Mysql one of connection params are undefined. [MYSQL_DB, MYSQL_USER, MYSQL_PASSWORD]');
        }

        return {
            host: MASTER_DB_HOST,
            port: MASTER_DB_PORT,
            database: MASTER_DB,
            user: MASTER_DB_USER,
            password: MASTER_DB_PASSWORD,
            queueLimit: MASTER_DB_QUEUE_LIMIT,
            pingInterval: MASTER_DB_PING_INTERVAL,
            connectionLimit: MASTER_DB_CONNECTION_LIMIT
        };
    }
};

