"use strict"
const chalk = require('chalk');
const {api} = require(__api);

const {utils} = api;
const cli = api.classes.cli;

function reset() {
    cli.call(this);
    this.name = "reset db";
}
reset.prototype = Object.create(cli.prototype);
var self = reset.prototype;

self.run = async function() {
    console.log(`test works`);
}

module.exports = reset;